/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.candybar;

/**
 *
 * @author caas1
 */
public class Pipoca implements Producto {
    private final String tamano;
    private final double precio;

    public Pipoca(String tamano, double precio) {
        this.tamano = tamano;
        this.precio = precio;
    }

    @Override
    public double getPrecio() {
        return precio;
    }

    public String getTamano() {
        return tamano;
    }
}