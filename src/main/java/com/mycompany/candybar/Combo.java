/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.candybar;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author caas1
 */

public class Combo implements Producto {
    private final List<Producto> productos = new ArrayList<>();

    public void agregarProducto(Producto producto) {
        productos.add(producto);
    }

    @Override
    public double getPrecio() {
        double precioTotal = 0;
        precioTotal = productos.stream().map(producto -> producto.getPrecio()).reduce(precioTotal, (accumulator, _item) -> accumulator + _item);
        return precioTotal - calcularDescuento();
    }

    private double calcularDescuento() {
        int cantidadPipocasMedianas = 0;
        int cantidadSodasMedianas = 0;

        for (Producto producto : productos) {
            if (producto instanceof Pipoca && ((Pipoca) producto).getTamano().equals("mediana")) {
                cantidadPipocasMedianas++;
            } else if (producto instanceof Soda && ((Soda) producto).getTamano().equals("mediana")) {
                cantidadSodasMedianas++;
            }
        }

        double descuento = 0;

        if (cantidadPipocasMedianas >= 2 && cantidadSodasMedianas >= 2) {
            // Combo 1: 1 bs menos
            descuento = 1;
        } else if (cantidadPipocasMedianas >= 1 && cantidadSodasMedianas >= 1) {
            // Combo 2: 0.5 bs menos
            descuento = 0.5;
        } else if (cantidadPipocasMedianas >= 2 && cantidadSodasMedianas >= 2) {
            // Combo 3: 2 bs menos
            descuento = 2;
        }

        return descuento;
    }
}
