/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.candybar;
import java.util.Scanner;

/**
 *
 * @author caas1
 */

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            // Solicitar productos al usuario
            System.out.println("Seleccione productos:");
            System.out.println("1. Pipoca pequeña (5 bs)");
            System.out.println("2. Pipoca mediana (8 bs)");
            System.out.println("3. Pipoca grande (10 bs)");
            System.out.println("4. Soda pequeña (4 bs)");
            System.out.println("5. Soda mediana (6 bs)");
            System.out.println("6. Soda grande (8 bs)");
            System.out.println("7. Combo 1");
            System.out.println("8. Combo 2");
            System.out.println("9. Combo 3");
            // Crea un combo para almacenar los productos seleccionados
            Combo combo = new Combo();
            while (true) {
                System.out.println("Ingrese el número del producto (0 para finalizar): ");
                int opcion = scanner.nextInt();                
                if (opcion == 0) {
                    break;
                }                
                switch (opcion) {
                    case 1 -> combo.agregarProducto(new Pipoca("pequeña", 5));
                    case 2 -> combo.agregarProducto(new Pipoca("mediana", 8));
                    case 3 -> combo.agregarProducto(new Pipoca("grande", 10));
                    case 4 -> combo.agregarProducto(new Soda("pequeña", 4));
                    case 5 -> combo.agregarProducto(new Soda("mediana", 6));
                    case 6 -> combo.agregarProducto(new Soda("grande", 8));
                    case 7 -> {
                        Combo combo1 = new Combo();
                        combo1.agregarProducto(new Pipoca("mediana", 8));
                        combo1.agregarProducto(new Pipoca("mediana", 8));
                        combo1.agregarProducto(new Soda("mediana", 6));
                        combo1.agregarProducto(new Soda("mediana", 6));
                        combo.agregarProducto(combo1);
                    }
                    case 8 -> {
                        Combo combo2 = new Combo();
                        combo2.agregarProducto(new Pipoca("mediana", 8));
                        combo2.agregarProducto(new Soda("mediana", 6));
                        combo.agregarProducto(combo2);
                    }
                    case 9 -> {
                        Combo combo3 = new Combo();
                        combo3.agregarProducto(new Pipoca("grande", 10));
                        combo3.agregarProducto(new Pipoca("grande", 10));
                        combo3.agregarProducto(new Soda("mediana", 6));
                        combo3.agregarProducto(new Soda("mediana", 6));
                        combo.agregarProducto(combo3);
                    }
                    default -> System.out.println("Opción no válida.");
                }
            }   // Calcular y mostrar el precio total
            double precioTotal = combo.getPrecio();
            System.out.println("Precio total: " + precioTotal + " bs");
            // Cierra el scanner
        }
    }
}
